from logging import debug
from flask import Flask, json, jsonify


cmd_history = []
exec_history = []


app = Flask(__name__)

@app.route("/")
def index():
    return "Hello, World"


@app.route("/get_command")
def get_command():
    if len(exec_history) <= 0:
        return jsonify({"cod": 404, "msg": "no new command!"})

    tmp = exec_history[0]
    exec_history.pop(0)

    if not tmp == "":
        cmd_history.append(tmp) 

    return jsonify({"cmd": tmp, "cod": 200})

@app.route("/get_history")
def get_cmd_history():
    return jsonify({"executed": cmd_history, "queue": exec_history, "cod": 200})

@app.route("/add_cmd/<cmd>")
def add_cmd(cmd):
    exec_history.append(cmd)
    return jsonify({"msg": "cmd added","cmds": exec_history, "cod": 200})



if __name__ == "__main__":
    app.run(host="0.0.0.0",port=6248, debug=True)
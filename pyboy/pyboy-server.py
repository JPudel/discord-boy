from pyboy import PyBoy
import pyboy
import time
import requests 
import threading

boyClient = PyBoy("./ROMs/SELECT_YOUR_ROM.gb")

ticks = 0 
request_per_ticks = 30

HOST = "0.0.0.0"
PORT = 6248



while not boyClient.tick():
    ticks = ticks +1
    # print(f"Ticks: {ticks}"
    if request_per_ticks < ticks:
        ticks = 0
        body = requests.get(f"http://{HOST}:{PORT}/get_command").json()
        

        boyClient.send_input(pyboy.WindowEvent.RELEASE_BUTTON_A)
        boyClient.send_input(pyboy.WindowEvent.RELEASE_BUTTON_B)
        boyClient.send_input(pyboy.WindowEvent.RELEASE_BUTTON_START)
        boyClient.send_input(pyboy.WindowEvent.RELEASE_BUTTON_SELECT)
        boyClient.send_input(pyboy.WindowEvent.RELEASE_ARROW_DOWN)
        boyClient.send_input(pyboy.WindowEvent.RELEASE_ARROW_UP)
        boyClient.send_input(pyboy.WindowEvent.RELEASE_ARROW_LEFT)
        boyClient.send_input(pyboy.WindowEvent.RELEASE_ARROW_RIGHT)


        if body["cod"] == 200:
        
            cmd = body["cmd"]
        
            if cmd == "button_a":
                print("Pressing A")
                boyClient.send_input(pyboy.WindowEvent.PRESS_BUTTON_A)
            if cmd == "button_b":
                print("Pressing B")
                boyClient.send_input(pyboy.WindowEvent.PRESS_BUTTON_B)
            if cmd == "button_start":
                print("Pressing START")
                boyClient.send_input(pyboy.WindowEvent.PRESS_BUTTON_START)
            if cmd == "button_select":
                print("Pressing SELECT")
                boyClient.send_input(pyboy.WindowEvent.PRESS_BUTTON_SELECT)
            
            if cmd == "button_up":
                print("Pressing up")
                boyClient.send_input(pyboy.WindowEvent.PRESS_ARROW_UP)
            if cmd == "button_down":
                print("Pressing DOWN")
                boyClient.send_input(pyboy.WindowEvent.PRESS_ARROW_DOWN)
            if cmd == "button_left":
                print("Pressing LEFT")
                boyClient.send_input(pyboy.WindowEvent.PRESS_ARROW_LEFT)
            if cmd == "button_right":
                print("Pressing RIGHT")
                boyClient.send_input(pyboy.WindowEvent.PRESS_ARROW_RIGHT)
            
    

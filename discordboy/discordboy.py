from os import name
import discord 
from discord.ext import commands
import requests

client = discord.Client()

HOST = "0.0.0.0"
PORT = 6248

msg = "LEFT  UP  RIGHT  DOWN  A  B  START  SELECT"
channel = None 

@client.event
async def on_ready():
    print(f"We have logged in as {client.user.name}")

@client.event
async def on_reaction_add(reaction, user):
    channel = reaction.message.channel
    print(reaction)

@client.event
async def on_raw_reaction_add(reaction):
    if reaction.emoji.name == "⏪":
        requests.get(f"http://{HOST}:{PORT}/add_cmd/button_left")
    if reaction.emoji.name == "⏫":
        requests.get(f"http://{HOST}:{PORT}/add_cmd/button_up")
    if reaction.emoji.name == "⏬":
        requests.get(f"http://{HOST}:{PORT}/add_cmd/button_down")
    if reaction.emoji.name == "⏩":
        requests.get(f"http://{HOST}:{PORT}/add_cmd/button_right")
    if reaction.emoji.name == "🅰":
        requests.get(f"http://{HOST}:{PORT}/add_cmd/button_a")
    if reaction.emoji.name == "🅱":
        requests.get(f"http://{HOST}:{PORT}/add_cmd/button_b")
    if reaction.emoji.name == "✅":
        requests.get(f"http://{HOST}:{PORT}/add_cmd/button_start")
    if reaction.emoji.name == "🚫":
        requests.get(f"http://{HOST}:{PORT}/add_cmd/button_select")
    

@client.event
async def on_message(message):

    if message.content.startswith(".summon"):
        channel = message.channel
        await channel.send(msg)

    if msg == message.content:
        await message.add_reaction("⏪")
        await message.add_reaction("⏫")
        await message.add_reaction("⏩")
        await message.add_reaction("⏬")
        await message.add_reaction("🅰")
        await message.add_reaction("🅱")
        await message.add_reaction("✅")
        await message.add_reaction("🚫")

    if message.content.startswith(".a"):
        requests.get(f"http://{HOST}:{PORT}/add_cmd/button_a")
        print("add button a")
    if message.content.startswith(".b"):
        requests.get(f"http://{HOST}:{PORT}/add_cmd/button_b")
        print("add button b")
    if message.content.startswith(".start"):
        requests.get(f"http://{HOST}:{PORT}/add_cmd/button_start")
        print("add button start")
    if message.content.startswith(".select"):
        requests.get(f"http://{HOST}:{PORT}/add_cmd/button_select")
        print("add button select")


@client.event
async def on_reaction(message):
    print(message)

client.run('DISCORD_TOKEN')
